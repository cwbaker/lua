#ifndef SWEET_LUA_ADDMEMBERHELPER_HPP_INCLUDED
#define SWEET_LUA_ADDMEMBERHELPER_HPP_INCLUDED

#include "declspec.hpp"

struct lua_State;

namespace sweet
{

namespace lua
{

class Lua;

class SWEET_LUA_DECLSPEC AddMemberHelper
{
    Lua* lua_;
    int references_;
    int restore_to_position_;
    
    public:
        AddMemberHelper();   
        lua_State* get_lua_state() const;     
        void create( Lua* lua );
        void destroy();
        void reference();
        void release();
};

}

}

#endif