//
// LuaWaitingThread.cpp
// Copyright (c) Charles Baker. All rights reserved.
//

#include "LuaWaitingThread.hpp"
#include <sweet/assert/assert.hpp>
#include <algorithm>

using std::find;
using std::swap;
using std::vector;
using namespace sweet::lua;

LuaWaitingThread::LuaWaitingThread( LuaThread* waiting_thread )
: waiting_thread_( waiting_thread ),
  waited_on_threads_()
{ 
    SWEET_ASSERT( waiting_thread_ );
}

LuaThread* LuaWaitingThread::waiting_thread() const
{
    return waiting_thread_;
}

bool LuaWaitingThread::ready() const
{
    return waited_on_threads_.empty();
}

bool LuaWaitingThread::resumed() const
{
    return waiting_thread_ == nullptr;
}

bool LuaWaitingThread::is_waiting_for_thread( LuaThread* thread ) const
{
    return find( waited_on_threads_.begin(), waited_on_threads_.end(), thread ) != waited_on_threads_.end();
}

bool LuaWaitingThread::operator==( const LuaThread* thread ) const
{
    return waiting_thread_ == thread;
}

void LuaWaitingThread::clear()
{
    waiting_thread_ = nullptr;
    waited_on_threads_.clear();
}

void LuaWaitingThread::reserve( int capacity )
{
    SWEET_ASSERT( capacity >= 0 );
    waited_on_threads_.reserve( capacity );
}

void LuaWaitingThread::add_thread( const LuaThread* thread )
{
    SWEET_ASSERT( thread );
    SWEET_ASSERT( find(waited_on_threads_.begin(), waited_on_threads_.end(), thread) == waited_on_threads_.end() );
    if ( find(waited_on_threads_.begin(), waited_on_threads_.end(), thread) == waited_on_threads_.end() )
    {
        waited_on_threads_.push_back( thread );
    }
}

void LuaWaitingThread::remove_thread( const LuaThread* thread )
{
    vector<const LuaThread*>::iterator i = find( waited_on_threads_.begin(), waited_on_threads_.end(), thread );
    if ( i != waited_on_threads_.end() )
    {
        swap( *i, waited_on_threads_.back() );
        waited_on_threads_.pop_back();
    }
}
