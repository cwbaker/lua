#ifndef SWEET_LUA_LUAWAITINGTHREAD_HPP_INCLUDED
#define SWEET_LUA_LUAWAITINGTHREAD_HPP_INCLUDED

#include <vector>

namespace sweet
{

namespace lua
{

class LuaThread;

/**
// A %Lua coroutine.
*/
class LuaWaitingThread
{
    LuaThread* waiting_thread_;
    std::vector<const LuaThread*> waited_on_threads_;

public:
    LuaWaitingThread( LuaThread* waiting_thread );
    LuaThread* waiting_thread() const;
    bool ready() const;
    bool resumed() const;
    bool is_waiting_for_thread( LuaThread* thread ) const;
    bool operator==( const LuaThread* thread ) const;
    void clear();
    void reserve( int capacity );
    void add_thread( const LuaThread* thread );
    void remove_thread( const LuaThread* thread );
};

}

}

#endif
