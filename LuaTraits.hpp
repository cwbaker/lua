#ifndef SWEET_LUA_LUATRAITS_HPP_INCLUDED
#define SWEET_LUA_LUATRAITS_HPP_INCLUDED

/**
// The macro to define a LuaTraits class that associates a C++ type with its
// Lua storage type - LuaByValue, LuaByReference, or LuaTypedPointer.
*/
#define SWEET_LUA_TYPE_CONVERSION( TYPE, STORAGE, TNAME ) namespace sweet \
{ \
\
namespace lua \
{ \
\
template <> struct LuaTraits<TYPE> \
{ \
    typedef lua::STORAGE storage_type; \
    static constexpr const char* name = TNAME; \
}; \
\
} \
\
} 

namespace sweet
{

namespace lua
{

/**
// @internal
//
// A tag type used as a trait to specify that a type should be stored in the
// Lua virtual machine by value as userdata.
*/
struct LuaByValue {};

/**
// @internal
//
// A tag type used as a trait to specify that a type should be stored in the
// Lua virtual machine by reference with a pointer stored in a table.
*/
struct LuaByReference {};

/**
// @internal
//
// A tag type used as a trait to specify that a type should be stored in the
// Lua virtual machine by reference as a pointer stored in userdata.
*/
struct LuaAsPointer {};

/**
// @internal
//
// Extract trait information for Lua from types.
*/
template <class Type>
struct LuaTraits
{
    typedef LuaByValue storage_type;
    static constexpr const char* name = nullptr;
};

class LuaObject;
template <>
struct LuaTraits<LuaObject>
{
    typedef LuaByReference storage_type;
};

}

}

#endif
